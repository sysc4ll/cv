// https://github.com/STRML/strml.net

import './main.css';

import header_html from 'raw-loader!./static/header.html';

let style, code, cv;
let pause_resume, skip_animation;

let style_text = [0, 1, 2].map((i) => require('raw-loader!./static/style_' + i + '.css').default);
import cv_text from 'raw-loader!./static/cv.txt';

let animation_skipped = false, paused = false, done = false;

const isDev = window.location.hostname === 'localhost';
const speed = isDev ? 30 : 30;

import Promise from 'bluebird';

import { default as writeChar,
    writeSimpleChar, handleChar} from './static/writeChar';

import replaceURLs from './static/replaceURLs';

import Markdown from 'markdown';
const md = Markdown.markdown.toHTML;

document.addEventListener('DOMContentLoaded', function(e) {
    set_header();
    get_elements();
    create_event_handlers();
    start_animation();
});

function set_header() {
    let header = document.getElementById('header');

    header.innerHTML = header_html;
}

function get_elements() {
    style = document.getElementById('style');
    code = document.getElementById('code');
    cv = document.getElementById('cv');
    pause_resume = document.getElementById('pause-resume');
    skip_animation = document.getElementById('skip-animation');
}

function create_event_handlers() {
    pause_resume.addEventListener('click', function(e) {
        e.preventDefault();

        if (paused == true) {
            pause_resume.textContent = "Pause |";
            paused = false;
        }

        else {
            pause_resume.textContent = "Resume |";
            paused = true;
        }
    });

    skip_animation.addEventListener('click', function(e) {
        e.preventDefault();

        animation_skipped = true;
    });
}

async function start_animation() {
    try {
        await write_to(code, style_text[0], 0, speed, true);
        await write_to(cv, cv_text, 0, speed, false);
        await write_to(code, style_text[1], 0, speed, true);
        create_cv_box();
        await Promise.delay(1000);
        await write_to(code, style_text[2], 0, speed, true);
    }

    catch(e) {
        if (e.message === "SKIP IT") skip_it();

        else throw e;
    }
}

let endOfSentence = /[\.\?\!]\s$/;
let comma = /\D[\,]\s$/;
let endOfBlock = /[^\/]\n\n$/;

async function write_to(el, message, index, interval, mirror) {
    if (animation_skipped) throw new Error('SKIP IT');

    let chars = message.slice(index, index + 1);
    index += 1;

    el.scrollTop = el.scrollHeight; // ensure we stay scrolled to the bottom.

    if (mirror) writeChar(el, chars, style);

    else writeSimpleChar(el, chars);

    // schedule another write.
    if (index < message.length) {
        let this_interval = interval;
        let this_slice = message.slice(index - 2, index + 1);

        if (comma.test(this_slice)) this_interval = interval * 30;

        if (endOfBlock.test(this_slice)) this_interval = interval * 50;

        if (endOfSentence.test(this_slice)) this_interval = interval * 70;

        do {
            await Promise.delay(this_interval);
        } while(paused === true);

        return write_to(el, message, index, interval, mirror);
    }
}

function create_cv_box() {
    if (cv.classList.contains('flipped')) return;

    cv.innerHTML = '<div class="text">' + replaceURLs(cv_text) + '</div>' +
        '<div class="md">' + replaceURLs(md(cv_text)) + '</div>';

    cv.classList.add('flipped');
    cv.scrollTop = 9999;

    // flippy floppy
    let flipping = 0;
    require('mouse-wheel')(cv, async function(dx, dy) {
        if (flipping) return;

        let flipped = cv.classList.contains('flipped');
        let half = (cv.scrollHeight - cv.clientHeight) / 2;
        let pastHalf = flipped ? cv.scrollTop < half : cv.scrollTop > half;

        // if we're past half, flip the el.
        if (pastHalf) {
            cv.classList.toggle('flipped');
            flipping = true;
            await Promise.delay(500);
            cv.scrollTop = flipped ? 0 : 9999;
            flipping = false;
        }

        // scroll. If we've flipped, flip the scroll direction.
        cv.scrollTop += (dy * (flipped ? -1 : 1));
    }, true);
}

function skip_it() {
    if (done) return;

    done = true;

    let txt = style_text.join('\n'); 
    style.textContent += txt;

    let html = "";
    for (let i = 0; i < txt.length; i++) html = handleChar(html, txt[i]);

    code.innerHTML = html;

    create_cv_box();
}

