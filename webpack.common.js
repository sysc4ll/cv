const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        app: './src/main.js',
    },

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js', 
        clean: true,
    },

    plugins: [
        new HtmlWebpackPlugin({ template: './src/index.html' }),
        new CopyPlugin({
            patterns:
            [
                { from: './src/static/', to: 'static' },
            ],
        }),
    ],

    module: {
        rules: [
            {
                test: /\.css$/i,
                exclude: [/static/],
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
};

